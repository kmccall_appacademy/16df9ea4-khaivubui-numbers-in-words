require 'byebug'
class Fixnum
  def in_words
    return convert_0_to_999999999999(self) if self < 1_000_000_000_000
    return string_trillions(self) if (self % 1_000_000_000_000).zero?
    string_trillions(self) + ' ' +
      convert_0_to_999999999999(self % 1_000_000_000_000)
  end

  def convert_0_to_999999999999(num)
    return convert_0_to_999999999(num) if num < 1_000_000_000
    return string_billions(num) if (num % 1_000_000_000).zero?
    string_billions(num) + ' ' + convert_0_to_999999999(num % 1_000_000_000)
  end

  def convert_0_to_999999999(num)
    return convert_0_to_999999(num) if num < 1_000_000
    return string_millions(num) if (num % 1_000_000).zero?
    string_millions(num) + ' ' + convert_0_to_999999(num % 1_000_000)
  end

  def convert_0_to_999999(num)
    return convert_0_to_999(num) if num < 1000
    return string_thousands(num) if (num % 1000).zero?
    string_thousands(num) + ' ' + convert_0_to_999(num % 1000)
  end

  def convert_0_to_999(num)
    return convert_0_to_99(num) if num < 100
    return string_hundreds(num / 100 * 100) if (num % 100).zero?
    string_hundreds(num / 100 * 100) + ' ' + convert_0_to_99(num % 100)
  end

  def convert_0_to_99(num)
    return single_digit_string(num) if (0..9).to_a.include? num
    return string_from_10_to_19(num) if (10..19).to_a.include? num
    return string_tens(num / 10 * 10) if (num % 10).zero?
    string_tens(num / 10 * 10) + ' ' + single_digit_string(num % 10)
  end

  def single_digit_string(num)
    number_strs = 'zero one two three four five six seven eight nine'
    number_strs.split[num]
  end

  def string_from_10_to_19(num)
    number_strs = 'ten eleven twelve thirteen fourteen fifteen sixteen
    seventeen eighteen nineteen'
    number_strs.split[num - 10]
  end

  def string_tens(num)
    tens_strs = 'zero ten twenty thirty forty fifty sixty seventy eighty ninety'
    tens_strs.split[num / 10]
  end

  def string_hundreds(num)
    number_strs = 'zero one two three four five six seven eight nine'
    number_strs.split[num / 100] + ' hundred'
  end

  def string_thousands(num)
    convert_0_to_999(num / 1000) + ' thousand'
  end

  def string_millions(num)
    convert_0_to_999999(num / 1_000_000) + ' million'
  end

  def string_billions(num)
    convert_0_to_999(num / 1_000_000_000) + ' billion'
  end

  def string_trillions(num)
    convert_0_to_999(num / 1_000_000_000_000) + ' trillion'
  end
end
